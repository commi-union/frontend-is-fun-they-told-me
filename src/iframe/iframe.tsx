import React, { FC } from 'react';

interface Props {
    message: string;
}


function IFrameComponent(props: Props) {
    return (
        <>
            <h1>Welcome {props.message}</h1>
        </>
    );
};

export default IFrameComponent;