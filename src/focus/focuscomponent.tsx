import React, { FC, useState } from 'react';

interface Props {
}

function FocusComponent(props: Props) {
    const [state, setState] = useState<number>(1);

    const inputFunction = () => <input
        onChange={event => {
            const nextValue = parseInt(event.target.value)
            setState(nextValue > 1 ? nextValue : 1)
        }}
        type='number'
        value={state} />

    var inputs: React.ReactElement[] = [];
    for (var i = 0; i < state; i++) {
        inputs.push(inputFunction())
    }

    return (<div
        style={{
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
            gap: '1em',
            padding: '1em'
        } as React.CSSProperties}>
        {state == 5 && inputFunction()}
        {inputs}
    </div>);
};

export default FocusComponent;