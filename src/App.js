import logo from './logo.svg';
import './App.css';

function Greeting({ name }) {
  return <h1>Hello, {name}!</h1>;
}

function App() {
  return (
    <div className="App">
      <Greeting name="QWE"/>
    </div>
  );
}

export default App;
